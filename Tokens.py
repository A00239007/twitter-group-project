import tweepy
import warnings
import sys, os
from vaderSentiment.vaderSentiment import SentimentIntensityAnalyzer
from flask import Flask, request, url_for, render_template, json, jsonify

app = Flask(__name__)

warnings.filterwarnings("ignore")

analyser = SentimentIntensityAnalyzer()


def sentiment_analyzer_scores(sentence):
    score = analyser.polarity_scores(sentence)
    return score


# Simon's tokens
simon_auth = tweepy.OAuthHandler('UpIUD0atrlr6yuWtYIYJaVfdI', 'p1v9CHaZuNsndnsFw4MG1GuegSIUVGOCu46xSFcqwHaNl63gNj')
simon_auth.set_access_token('2941433217-UvN9r67nwCwlS6cLZ1phVTtCefRODpx9geetH9D',
                            '3IDdFfECS9LvZnnZELrB21WEISgCcLDKXCTYawoInLR4J')

# Marks tokens
mark_auth = tweepy.OAuthHandler('BgwgnwPss9rhIkf49jfkahkD5', 'SuinjTRVCY5nlFHqlVlicgFUE1mwd3kUL4UNHQJMcvMN8ssPIj')
mark_auth.set_access_token('2449939644-G0STDMshCDPdhj7Jc45pMcytfNYaApH3SIHfgpD',
                           'oloUdNd64uHqkXRPLMMqtYrvjr1jK208vTTmPyYxWor7J')

# Fei's tokens
fei_auth = tweepy.OAuthHandler('aOPao3tbHlMQw9OjhXsVn8huV', 'F2yefwSJ0kZLimvu5o8gnNPQtGnHtCVl0gt8Bw4jvv8yvzW3wR')
fei_auth.set_access_token('1173509583303827456-TenvOhSRbbydLR8l5iKthfZ3ycyRpG',
                          'XejGRMFPoiRuEPIk7q9fzfaGA9av7ojOI4XGcz76vx0GN')

# simon_api = tweepy.API(simon_auth)

# mark_api = tweepy.API(mark_auth)

fei_api = tweepy.API(fei_auth)

# apis = [simon_api, mark_api, fei_api]


# <<<<<<< HEAD
public_tweets = fei_api.home_timeline()

# for tweet in tweepy.Cursor(simon_api.search, q='#Survivor39').items(10):
#   sentiment_analyzer_scores(tweet.text)

# Global Variables for topics
topic1 = ""
topic2 = ""
staticTopic3 = ""

# Arrays storing the results of each tweet
dataTopicOne = []
dataTopicTwo = []
dataStaticTopic3 = []


def sentimentCheck(sentiment):
    sentimentValue = ""
    if sentiment['compound'] > 0:
        sentimentValue = "pos"
    if sentiment['compound'] < 0:
        sentimentValue = "neg"
    if sentiment['compound'] == 0:
        sentimentValue = "neu"
    return sentimentValue


class MyStreamListener(tweepy.StreamListener):

    def on_status(self, status):
        if "RT" not in status.text:
            text = status.text
            if topic1.lower() in text.lower():
                sentiment = sentiment_analyzer_scores(text)
                followers = status.user.followers_count
                user = status.user.screen_name
                sentimentValue = sentimentCheck(sentiment)
                data = [sentimentValue, followers, user]
                dataTopicOne.append(data)
            if topic2 in status.text:
                sentiment = sentiment_analyzer_scores(text)
                followers = status.user.followers_count
                user = status.user.screen_name
                sentimentValue = sentimentCheck(sentiment)
                data = [sentimentValue, followers, user]
                dataTopicTwo.append(data)


class StaticTweets():

        def on_status(self, status):
            if "RT" not in status.text:
                text = status.text
                print("hi")
                if staticTopic3.lower() in text.lower():
                    dataStaticTopic3.clear()
                    sentimentValue = sentiment_analyzer_scores(text)
                    followers = status.user.followers_count
                    retweets = status.retweet_count
                    likes = status.favorite_count
                    user = status.user.screen_name
                    data = [sentimentValue, followers, user, retweets, likes]
                    dataStaticTopic3.append(data)


myStreamListener = MyStreamListener()
myStream = tweepy.Stream(auth=fei_api.auth, listener=myStreamListener)


@app.route('/')
def index():
    return render_template('index.html')


@app.route('/get_tweets', methods=['GET'])
def get_tweets():
    global topic1, topic2
    if request.method == 'GET':
        topic1 = request.args.get('topic1')
        topic2 = request.args.get('topic2')
        myStream.filter(track=['#' + topic1 + '', '#' + topic2 + ''])


@app.route('/load_tweets', methods=['GET'])
def load_tweets():
    if request.method == 'GET':
        data = [dataTopicOne, dataTopicTwo, dataStaticTopic3]
        return jsonify(data)


@app.route('/stop_tweets', methods=['GET'])
def stop_tweets():
    myStream.disconnect()
    dataTopicOne.clear()
    dataTopicTwo.clear()
    return jsonify("Stream Stopped")


@app.route('/get_static_tweets', methods=['GET'])
def get_static_tweets():
    if request.method == 'GET':
        for tweet in tweepy.Cursor(fei_api.search, q='#Survivor39').items(20):
            if "RT" not in tweet.text:
                text = tweet.text
                if staticTopic3.lower() in text.lower():
                    sentimentValue = sentiment_analyzer_scores(text)
                    sentiment = sentimentCheck(sentimentValue)
                    followers = tweet.user.followers_count
                    retweets = tweet.retweet_count
                    likes = tweet.favorite_count
                    user = tweet.user.screen_name
                    data = [sentiment, followers, user, retweets, likes]
                    dataStaticTopic3.append(data)
    return jsonify("Successful")



if __name__ == '__main__':
    app.run()

# >>>>>>> simon_dev
