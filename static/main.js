//$SCRIPT_ROOT = {{ request.script_root|tojson|safe }};
$(document).ready(function(){
    var chartOne = document.getElementById("topic1PieChart").getContext("2d");
    var chartTwo = document.getElementById("topic2PieChart").getContext("2d");
    var chartThree = document.getElementById("topic3PieChart").getContext("2d");
    var barChart = document.getElementById("staticBar").getContext("2d");
    var barChart2 = document.getElementById("staticBar2").getContext("2d");

    var pieOne = new Chart(chartOne, {
        // The type of chart we want to create
        type: 'pie',

        // The data for our dataset
        data: {
            labels: ['Positive', 'Negative', 'Neutral'],
            datasets: [{
                label: 'My First dataset',
                backgroundColor: [
                    'rgb(44, 245, 181)',
                    'rgb(245, 44, 138)',
                    'rgb(44, 245, 245)'
                ],
                borderColor: 'rgb(0, 0, 0)',
                followers: [0,0,0],
                data: [0,0,0]
            }]
        },

        // Configuration options go here
        options: {
            //onClick: graphClickEvent
        }
    });




    var pieTwo = new Chart(chartTwo, {
        // The type of chart we want to create
        type: 'pie',

        // The data for our dataset
        data: {
            labels: ['Positive', 'Negative', 'Neutral'],
            datasets: [{
                label: 'My Second dataset',
                backgroundColor: [
                    'rgb(44, 245, 181)',
                    'rgb(245, 44, 138)',
                    'rgb(44, 245, 245)'
                ],
                borderColor: 'rgb(0, 0, 0)',
                followers: [0,0,0],
                data: [0,0,0]
            }]
        },

        // Configuration options go here
        options: {}
    });

    var pieThree = new Chart(chartThree, {
        // The type of chart we want to create
        type: 'pie',

        // The data for our dataset
        data: {
            labels: ['Positive', 'Negative', 'Neutral'],
            datasets: [{
                label: 'My Static dataset',
                backgroundColor: [
                    'rgb(44, 245, 181)',
                    'rgb(245, 44, 138)',
                    'rgb(44, 245, 245)'
                ],
                borderColor: 'rgb(0, 0, 0)',
                followers: [0,0,0],
                data: [0,0,0],
                retweets: [0,0,0],
                likes: [0,0,0]
            }]
        },

        // Configuration options go here
        options: {}
    });


    var myBarChart = new Chart(barChart, {
        type: 'bar',
        data: {
            labels: ["Retweets", "Likes"],
            datasets: [{
                label: 'Retweets and Likes',
                backgroundColor: [
                    'rgb(44, 245, 181)',
                    'rgb(245, 44, 138)'
                ],
                barPercentage: 0.5,
                barThickness: 6,
                maxBarThickness: 8,
                minBarLength: 100,
                data: [0,0],
            }]
        },
    });
    var myBarChartTwo = new Chart(barChart2, {
        type: 'bar',
        data: {
            labels: ["Followers"],
            datasets: [{
                label: 'Followers',
                backgroundColor: [
                    'rgb(44, 245, 181)'
                ],
                barPercentage: 0.5,
                barThickness: 6,
                maxBarThickness: 8,
                minBarLength: 100,
                data: [0],
            }]
        },
    });
    $("#topic3PieChart").click(function(evt){
        var activePoints = pieThree.getElementsAtEvent(evt);
        if(activePoints.length > 0){
          //get the internal index of slice in pie chart
          var clickedElementindex = activePoints[0]["_index"];

          //get specific label by index
          var label = pieThree.data.labels[clickedElementindex];

          //get value by index
          var value = pieThree.data.datasets[0].data[clickedElementindex];
          if(label == "Positive"){
                myBarChartTwo.data.datasets[0].data[0] = pieThree.data.datasets[0].followers[0];
                myBarChart.data.datasets[0].data[0] = pieThree.data.datasets[0].retweets[0];
                myBarChart.data.datasets[0].data[1] = pieThree.data.datasets[0].likes[0];
          }else if(label == "Negative"){
                myBarChartTwo.data.datasets[0].data[0] = pieThree.data.datasets[0].followers[1];
                myBarChart.data.datasets[0].data[0] = pieThree.data.datasets[0].retweets[1];
                myBarChart.data.datasets[0].data[1] = pieThree.data.datasets[0].likes[1];
          }else if(label == "Neutral"){
                myBarChartTwo.data.datasets[0].data[0] = pieThree.data.datasets[0].followers[2];
                myBarChart.data.datasets[0].data[0] = pieThree.data.datasets[0].retweets[2];
                myBarChart.data.datasets[0].data[1] = pieThree.data.datasets[0].likes[2];
          }
           myBarChart.update();
           myBarChartTwo.update();
           $('#modalHeading').text(label+' Drill Down')
           $('#mainModal').modal('show');
       }
    });
    $("#topic1PieChart").click(function(evt){
        var activePoints = pieOne.getElementsAtEvent(evt);
        if(activePoints.length > 0){
          //get the internal index of slice in pie chart
          var clickedElementindex = activePoints[0]["_index"];

          //get specific label by index
          var label = pieOne.data.labels[clickedElementindex];

          //get value by index
          var value = pieOne.data.datasets[0].data[clickedElementindex];
          console.log(pieOne.data.datasets[0].followers)
          if(label == "Positive"){
                myBarChartTwo.data.datasets[0].data[0] = pieOne.data.datasets[0].followers[0];
                myBarChart.data.datasets[0].data[0] = 0;
                myBarChart.data.datasets[0].data[1] = 0;
          }else if(label == "Negative"){
                myBarChartTwo.data.datasets[0].data[0] = pieOne.data.datasets[0].followers[1];
                myBarChart.data.datasets[0].data[0] = 0;
                myBarChart.data.datasets[0].data[1] = 0;
          }else if(label == "Neutral"){
                myBarChartTwo.data.datasets[0].data[0] = pieOne.data.datasets[0].followers[2];
                myBarChart.data.datasets[0].data[0] = 0;
                myBarChart.data.datasets[0].data[1] = 0;
          }
           myBarChartTwo.update();
           myBarChart.update();
           $('#modalHeading').text(label+' Drill Down')
           $('#mainModal').modal('show');
       }
    });
    $("#topic2PieChart").click(function(evt){
        var activePoints = pieTwo.getElementsAtEvent(evt);
        if(activePoints.length > 0){
          //get the internal index of slice in pie chart
          var clickedElementindex = activePoints[0]["_index"];

          //get specific label by index
          var label = pieTwo.data.labels[clickedElementindex];

          //get value by index
          var value = pieTwo.data.datasets[0].data[clickedElementindex];
          if(label == "Positive"){
                myBarChartTwo.data.datasets[0].data[0] = pieThree.data.datasets[0].followers[0];
                myBarChart.data.datasets[0].data[0] = 0;
                myBarChart.data.datasets[0].data[1] = 0;
          }else if(label == "Negative"){
                myBarChartTwo.data.datasets[0].data[0] = pieThree.data.datasets[0].followers[1];
                myBarChart.data.datasets[0].data[0] = 0;
                myBarChart.data.datasets[0].data[1] = 0;
          }else if(label == "Neutral"){
                myBarChartTwo.data.datasets[0].data[0] = pieThree.data.datasets[0].followers[2];
                myBarChart.data.datasets[0].data[0] = 0;
                myBarChart.data.datasets[0].data[1] = 0;
          }
           myBarChartTwo.update();
           myBarChart.update();
           $('#mainModal').modal('show');
           $('#modalHeading').text(label+' Drill Down')
       }
    });
    setInterval(function(){
       $.ajax({
        type : "GET",
        url : '/load_tweets',
        dataType: "json",
        contentType: 'application/json;charset=UTF-8',
        success: function (data) {
            var numbersOne = [0,0,0];
            var numbersTwo = [0,0,0];
            var numbersThree = [0,0,0];
            var followerOne = [0,0,0];
            var followerTwo = [0,0,0];
            var followerThree = [0,0,0];
            var retweetsOne = [0,0,0];
            var retweetsTwo = [0,0,0];
            var retweetsThree = [0,0,0];
            var likesOne = [0,0,0];
            var likesTwo = [0,0,0];
            var likesThree = [0,0,0];
            for(var i = 0; i<data[0].length; i++){
                if(data[0][i][0] == "pos"){
                    numbersOne[0]++;
                    followerOne[0]+=data[0][i][1];
                }else if(data[0][i][0] == "neg"){
                    numbersOne[1]++;
                    followerOne[1]+=data[0][i][1];
                }else{
                    numbersOne[2]++;
                    followerOne[2]+=data[0][i][1];
                }
            }
            for(var p = 0; p<data[1].length; p++){
                if(data[1][p][0] == "pos"){
                        numbersTwo[0]++;
                        followerTwo[0]+=data[1][p][1];
                }else if(data[1][p][0] == "neg"){
                    numbersTwo[1]++;
                    followerTwo[1]+=data[1][p][1];
                }else{
                    numbersTwo[2]++;
                    followerTwo[2]+=data[1][p][1];
                }
            }
            for(var s = 0; s<data[2].length; s++){
                if(data[2][s][0] == "pos"){
                        numbersThree[0]++;
                        followerThree[0]+=data[2][s][1];
                        retweetsThree[0]+=data[2][s][3];
                        likesThree[0]+=data[2][s][4];
                }else if(data[2][s][0] == "neg"){
                    numbersThree[1]++;
                    followerThree[1]+=data[2][s][1];
                    retweetsThree[1]+=data[2][s][3];
                    likesThree[1]+=data[2][s][4];
                }else{
                    numbersThree[2]++;
                    followerThree[2]+=data[2][s][1];
                    retweetsThree[2]+=data[2][s][3];
                    likesThree[2]+=data[2][s][4];
                }
            }
            pieOne.data.datasets.forEach((dataset) => {
                dataset.data.forEach(function(element, index){
                    this[index] = numbersOne[index];
                }, dataset.data)
                dataset.followers.forEach(function(element, index){
                    this[index] = followerOne[index];
                }, dataset.followers)
            });
            pieTwo.data.datasets.forEach((dataset) => {
                dataset.data.forEach(function(element, index){
                    this[index] = numbersTwo[index];
                }, dataset.data)
                dataset.followers.forEach(function(element, index){
                    this[index] = followerTwo[index];
                }, dataset.followers)
            });
            pieThree.data.datasets.forEach((dataset) => {
                dataset.data.forEach(function(element, index){
                    this[index] = numbersThree[index];
                }, dataset.data)
                dataset.followers.forEach(function(element, index){
                    this[index] = followerThree[index];
                }, dataset.followers)
                dataset.retweets.forEach(function(element, index){
                    this[index] = retweetsThree[index];
                }, dataset.retweets)
                dataset.likes.forEach(function(element, index){
                    this[index] = likesThree[index];
                }, dataset.likes)
            });
            pieOne.update();
            pieTwo.update();
            pieThree.update();
            }
        });
    }, 5000)
});

function get_tweets(){
    var topics = {
        topic1: $('#inputText1').val(),
        topic2: $('#inputText2').val()
    };
    $('#topicOneHeading').text(topics.topic1)
    $('#topicTwoHeading').text(topics.topic2)
    $('#charts').css('visibility','visible');
    $.ajax({
	type : "GET",
	url : '/get_tweets',
	dataType: "json",
	data: topics,
	contentType: 'application/json;charset=UTF-8',
	success: function (data) {
		console.log(data);
		}
	});
};

function stop_tweets(){
    $('#topicOneHeading').text('')
    $('#topicTwoHeading').text('')
    $('#charts').css('visibility','hidden');
    $.ajax({
	type : "GET",
	url : '/stop_tweets',
	dataType: "json",
	contentType: 'application/json;charset=UTF-8',
	success: function (data) {
		console.log(data);
		}
	});
};

function get_static_tweets(){
    $('#static_charts').css('visibility','visible');
    $.ajax({
	type : "GET",
	url : '/get_static_tweets',
	dataType: "json",
	contentType: 'application/json;charset=UTF-8',
	success: function (data) {
		console.log(data);
		}
	});
};
